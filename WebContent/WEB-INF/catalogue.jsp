<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous" />
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
header nav a {
	color: white;
}

main {
	height: 100vh;
	justify-content: center;
	align-items: center;
}
</style>
<title>Catalogue</title>
</head>
<body>

	<jsp:include page="navbar.jsp" />

	<main>

	<div>
		<h2>Aliments</h2>
		<div class="border">
			<div class="card-group">

				<c:forEach var="a" items="${halfAlimentsA}">

					<div class="card" style="width: 25rem; margin: 1rem">
						<img class="card-img-top" src="<c:out value='${a.url}' />"
							alt="<c:out value='${a.nom}' />"
							style="max-width: 400px; max-height: 400px;" />
						<div class="card-body">
							<h5 class="card-title">
								<a href="#" style="text-decoration: none; color: black"
									data-target="#description${a.ref}" data-toggle="collapse"><c:out
										value='${a.nom}' /> - <c:out value='${a.tarif}' /> € </a>
							</h5>
							<p class='card-text collapse' id="description${a.ref}">
								<c:out value='${a.description}' />
							</p>
						</div>
						<!-- fin div CARD-BODY -->
					</div>
					<!-- fin div CARD -->

				</c:forEach>
			</div>
			<!-- fin div CARD-GROUP -->

			<div class="card-group">

				<c:forEach var="a" items="${halfAlimentsB}">

					<div class="card" style="width: 25rem; margin: 1rem">
						<img class="card-img-top" src="<c:out value='${a.url}' />"
							alt="<c:out value='${a.nom}' />"
							style="max-width: 400px; max-height: 400px;" />
						<div class="card-body">
							<h5 class="card-title">
								<a href="#" style="text-decoration: none; color: black"
									data-target="#description${a.ref}" data-toggle="collapse"><c:out
										value='${a.nom}' /> - <c:out value='${a.tarif}' /> € </a>
							</h5>
							<p class='card-text collapse' id="description${a.ref}">
								<c:out value='${a.description}' />
							</p>
						</div>
						<!-- fin div CARD-BODY -->
					</div>
					<!-- fin div CARD -->

				</c:forEach>
			</div>
			<!-- fin div CARD-GROUP -->


		</div>
		<!-- fin div BORDER-->
	</div>
	<!-- fin div  ALIMENTS -->


	<div>
		<h2>Boissons</h2>
		<div class="border">
			<div class="card-group">

				<c:forEach var="a" items="${halfBoissonsA}">

					<div class="card" style="width: 25rem; margin: 1rem">
						<img class="card-img-top" src="<c:out value='${a.url}' />"
							alt="<c:out value='${a.nom}' />"
							style="max-width: 400px; max-height: 400px;" />
						<div class="card-body">
							<h5 class="card-title">
								<a href="#" style="text-decoration: none; color: black"
									data-target="#description${a.ref}" data-toggle="collapse"><c:out
										value='${a.nom}' /> - <c:out value='${a.tarif}' /> € </a>
							</h5>
							<p class='card-text collapse' id="description${a.ref}">
								<c:out value='${a.description}' />
							</p>
						</div>
						<!-- fin div CARD-BODY -->
					</div>
					<!-- fin div CARD -->

				</c:forEach>
			</div>
			<!-- fin div CARD-GROUP -->

			<div class="card-group">

				<c:forEach var="a" items="${halfBoissonsB}">

					<div class="card" style="width: 25rem; margin: 1rem">
						<img class="card-img-top" src="<c:out value='${a.url}' />"
							alt="<c:out value='${a.nom}' />"
							style="max-width: 400px; max-height: 400px;" />
						<div class="card-body">
							<h5 class="card-title">
								<a href="#" style="text-decoration: none; color: black"
									data-target="#description${a.ref}" data-toggle="collapse"><c:out
										value='${a.nom}' /> - <c:out value='${a.tarif}' /> € </a>
							</h5>
							<p class='card-text collapse' id="description${a.ref}">
								<c:out value='${a.description}' />
							</p>
						</div>
						<!-- fin div CARD-BODY -->
					</div>
					<!-- fin div CARD -->

				</c:forEach>
			</div>
			<!-- fin div CARD-GROUP -->



		</div>
		<!-- fin div BORDER-->
	</div>
	<!-- fin div  ALIMENTS -->

	<div>
		<h2>Ménagers</h2>
		<div class="border">
			<div class="card-group">

				<c:forEach var="a" items="${halfMenagersA}">

					<div class="card" style="width: 25rem; margin: 1rem">
						<img class="card-img-top" src="<c:out value='${a.url}' />"
							alt="<c:out value='${a.nom}' />"
							style="max-width: 400px; max-height: 400px;" />
						<div class="card-body">
							<h5 class="card-title">
								<a href="#" style="text-decoration: none; color: black"
									data-target="#description${a.ref}" data-toggle="collapse"><c:out
										value='${a.nom}' /> - <c:out value='${a.tarif}' /> € </a>
							</h5>
							<p class='card-text collapse' id="description${a.ref}">
								<c:out value='${a.description}' />
							</p>
						</div>
						<!-- fin div CARD-BODY -->
					</div>
					<!-- fin div CARD -->

				</c:forEach>
			</div>
			<!-- fin div CARD-GROUP -->

			<div class="card-group">

				<c:forEach var="a" items="${halfMenagersB}">

					<div class="card" style="width: 25rem; margin: 1rem">
						<img class="card-img-top" src="<c:out value='${a.url}' />"
							alt="<c:out value='${a.nom}' />"
							style="max-width: 400px; max-height: 400px;" />
						<div class="card-body">
							<h5 class="card-title">
								<a href="#" style="text-decoration: none; color: black"
									data-target="#description${a.ref}" data-toggle="collapse"><c:out
										value='${a.nom}' /> - <c:out value='${a.tarif}' /> € </a>
							</h5>
							<p class='card-text collapse' id="description${a.ref}">
								<c:out value='${a.description}' />
							</p>
						</div>
						<!-- fin div CARD-BODY -->
					</div>
					<!-- fin div CARD -->

				</c:forEach>
			</div>
			<!-- fin div CARD-GROUP -->

		</div>
		<!-- fin div BORDER-->
	</div>
	<!-- fin div  ALIMENTS --> </main>



	<!-- footer -->
	<jsp:include page="footer.jsp" />



</body>
</html>