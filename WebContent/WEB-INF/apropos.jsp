<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>A propos de nous</title>
		
		<style>
			main {
				display: flex;
				height: 80vh;
				justify-content: center;
				align-items: center;
			}
		</style>
	</head>

	<body>
	
		<!-- Header -->
		<jsp:include page="navbar.jsp" />
	
		<main>
			<div class="card-group">
				<div class="card"
					style="width: 18rem; margin: 2rem; text-align: center;">
					<div class="card-body"
						style="border: 2px solid blue; border-radius: 5%;">
						<h5 class="card-title">David Durand</h5>
						<h6 class="card-subtitle mb-2 text-muted">Position 1</h6>
						<a href="#" class="card-link">LinkedIn</a>
					</div>
				</div>
			</div>
			<div class="card-group">
				<div class="card"
					style="width: 18rem; margin: 2rem; text-align: center;">
					<div class="card-body"
						style="border: 2px solid blue; border-radius: 5%;">
						<h5 class="card-title">Corentin Dumontier</h5>
						<h6 class="card-subtitle mb-2 text-muted">Position 2</h6>
						<a href="#" class="card-link">LinkedIn</a>
					</div>
				</div>
			</div>
			<div class="card-group">
				<div class="card"
					style="width: 18rem; margin: 2rem; text-align: center;">
					<div class="card-body"
						style="border: 2px solid blue; border-radius: 5%;">
						<h5 class="card-title">Antoine Tassin</h5>
						<h6 class="card-subtitle mb-2 text-muted">Position 3</h6>
						<a href="#" class="card-link">LinkedIn</a>
					</div>
				</div>
			</div>
		</main>
		
		<!-- footer -->
	    <jsp:include page="footer.jsp" />
	
	</body>
</html>