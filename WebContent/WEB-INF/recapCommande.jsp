<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>recapCommande</title>
</head>

<body>

	<!-- Header -->
	<jsp:include page="navbar.jsp" />

	<div style="height: 150px;"></div>

	<main>


	<h3 class="text-center">Récapitulatif de la commande de 
	<c:out value="${sessionScope.client.prenom}"/> 
	<c:out value="${sessionScope.client.nom}"/> 
	(Montant panier : <c:out value="${sessionScope.facture.totalFacture}"/>)</h3>


	<div class="container mt-5">

		<div class="row">

			<table class="table">
				<thead>
					<tr>
						<th scope="col">Article</th>
						<th scope="col">QuantitÃ©</th>
						<th scope="col">Prix total</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="ligne" items="${sessionScope.facture.lignes}">
						<tr>
							<td><c:out value="${ligne.article.nom}"/></td>
							<td><c:out value="${ligne.quantite}"/></td>
							<td><c:out value="${ligne.totalLigne}"/></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>

			</div>
		</div>

	<div class="container">
		<div class="d-flex flex-row mt-3 justify-content-between">
			<form action="RootagePage?destination=maCommande" method="post">
				<input type="submit" value="Retour au panier" class="btn btn-primary">
			</form>

			<form action="CreerCommandeDansBase" method="post" > 
				 <input type="submit" value="Valider la commande" class="btn btn-primary">
			</form>
		</div>
		</div>
		
		
	</main>

	<div style="height: 100px;" class="float-none"></div>

	<!-- footer -->
	<jsp:include page="footer.jsp" />


</body>

</html>