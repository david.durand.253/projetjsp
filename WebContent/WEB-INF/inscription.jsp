<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous" />
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
header nav a {
	color: white;
}

h2{
text-align:center;
}
main {
	margin-left:15vw;
	height: 100vh;
	width:70vw;
	
}
</style>
<title>Inscription</title>
</head>
<body>
<!-- Header -->
<jsp:include page="navbar.jsp" />

<main>

	<br>
	<h2>Inscription au site</h2>
	<br><br>
	
	<form  action="inscriptionCompte" method="post">
		
		<label for="identifiant" class="form-label">Identifiant</label>
		<input type="number" class="form-control" id="identifiant" name="identifiant" placeholder="Inscrivez un identifiant" /><br>
		
		<label for="nom" class="form-label">Nom</label>
		<input type="text" class="form-control" id="nom" name="nom" placeholder="Inscrivez votre nom" /><br>
		
		<label for="prenom" class="form-label">Prenom</label>
		<input type="text" class="form-control" id="prenom" name="prenom" placeholder="Inscrivez votre prenom" /><br>
		
		<label for="password" class="form-label">Mot de Passe</label>
		<input type="password" class="form-control" id="password" name="password" placeholder="Inscrivez un mot de passe" /><br>
		
		<label for="adresse" class="form-label">Adresse</label>
		<input type="text" class="form-control" id="adresse" name="adresse" placeholder="Inscrivez votre adresse" /><br>
		
		<label for="telephone" class="form-label">Telephone</label>
		<input type="text" class="form-control" id="telephone" name="telephone" placeholder="Inscrivez votre telephone" /><br><br>
		
		<button class="btn btn-primary" name="submit" value="submit">Soumettre</button>
	</form>


</main>

   <!-- footer -->
    <jsp:include page="footer.jsp" />

</body>
</html>