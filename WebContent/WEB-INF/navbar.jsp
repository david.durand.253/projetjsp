<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		
		<!-- bootstrap -->
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	
		<style>
			nav a {
				color: white;
			}
		</style>
		
	
	</head>
	
	<body>
	
		<nav class="navbar navbar-expand bg-primary fixed-top">
			<div class="container-fluid ">
				<!-- container-fluid suffit � la r�partition gauche - centre - droite des diff�rentes <ul> -->
	
				<ul class="navbar-nav ">
					<li class="nav-item">
						<a class="nav-link " href="RootagePage">
							<img style="width: 80px;" src="img/nourriture/burger.png">
						</a>
				</ul>
	
				<!-- Categorie catalogue -->
				<ul class="navbar-nav">
					<li class="nav-item "><a class="nav-link" href="RootagePage?destination=catalogue">Catalogue</a>
				</ul>
	
				<!-- Categorie connection -->
				<ul class="navbar-nav">

					<c:if test="${sessionScope.estConnecte == false}">
						<li class="nav-item "><a class="nav-link" href="RootagePage?destination=connection">Se connecter</a>
						<li class="nav-item "><a class="nav-link" href="RootagePage?destination=inscription">Inscription</a>
					</c:if>
					<c:if test="${sessionScope.estConnecte == true}">
						<li class="nav-item "><a class="nav-link" href="Deconnection">Se d�connecter</a>
						<li class="nav-item "><a class="nav-link" href="RootagePage?destination=maCommande">Panier</a>
					</c:if>

				</ul>
			</div>
	
		</nav>
			
		<div style="height: 100px;"></div>
	</body>
</html>