<%@ page isELIgnored="false" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="model.Ligne" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>maCommande</title>
</head>

<body>

	<!-- Header -->
	<jsp:include page="navbar.jsp" />

	<div style="height: 150px;"></div>

	<main>

	<h2 class="text-center">Constitue ta commande !</h2>

	<h3 class="text-center">
		La commande de
		<c:out value='${client.prenom}' />
		<c:out value='${client.nom}' />
		(Montant panier : ${facture.totalFacture})
	</h3>


	<div class="container mt-5">

		<div class="row">

			<div class="col-md-6 w-100">

				<p class="font-weight-bold mt-3">Fais ton choix parmi les items
					du catalogue</p>

				<form action="ajoutPanier" method="post" class="pt-3">
					<select name="listeDeroul" id="listeDeroul" class="w-100">
						<option value="">--Choisis ton article--</option>
						<c:forEach var="a" items="${toutCatalogue}">
							<option value="<c:out value='${a.ref}' />">
								<c:out value='${a.nom}' />
							</option>
						</c:forEach>
					</select> <label for="quantite">Quantité : </label> <input type="number"
						name="quantite" class="w-25" required> <input
						type="submit" value="Ajouter au panier" class="btn btn-primary">
				</form>

			</div>

			<div class="col-md-6 w-100">
				<table class="table text-center">
					<thead>
						<tr>
							<th scope="col">Article</th>
							<th scope="col">Quantité</th>
							<th scope="col">Prix total</th>
						</tr>
					</thead>
					<tbody>

						<c:forEach var="a" items="${facture.lignes}">
							<tr>
								<td>${a.article.nom}</td>
								<td style="display: flex; justify-content: space-between;">

									<form action="Moins?ref=${a.article.ref}&quantite=${a.quantite}" method="post">
										<input class="btn btn-outline-primary btn-sm" type="submit"
											value="-">
									</form> ${a.quantite}
									<form action="Plus?ref=${a.article.ref}&quantite=${a.quantite}" method="post">
										<input class="btn btn-outline-primary btn-sm" type="submit"
											value="+">
									</form>
								</td>
								<td>${a.totalLigne}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>


			</div>

		</div>

	</div>

	<div class="d-flex flex-row-reverse mt-3 mr-5">
		<form action="RootagePage?destination=recapCommande" method="post">
			<input type="submit" value="Valider le panier"
				class="btn btn-primary">
		</form>
	</div>

	</main>

	<div style="height: 100px;" class="float-none"></div>

	<!-- footer -->
	<jsp:include page="footer.jsp" />

</body>

</html>