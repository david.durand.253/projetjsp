<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>

	<div style="height: 100px;"></div>

	<footer>
			<nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-bottom">
				<div class="container-fluid ">
	
					<ul class="navbar-nav ">
						<li class="nav-item">
							<a href="RootagePage?destination=apropos" class="text-white">A propos</a>
					</ul>
	
	
					<ul class="navbar-nav ">
						<li class="nav-item"><a href="#" class="text-white">
								JusteManger is a property of AJC-Groupe1 Co.,Ltd. �2023 All
								Rights Reserved</a>
					</ul>
	
					<ul class="navbar-nav ">
						<li class="nav-item">
							<a href="RootagePage?destination=contact" class="text-white">Nous contacter</a>
					</ul>
	
				</div>
			</nav>
	</footer>

</body>
</html>