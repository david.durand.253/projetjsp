<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Nous contacter</title>
		
		<!-- bootstrap -->
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	
	</head>

	<body>
	
		<!-- Header -->
		<jsp:include page="navbar.jsp" />
	
		<form class="container" action="index.html">
	
			<br>
			<br>
	
			<div class="form-group">
	
				<input type="email" class="form-control" id="email1"
					aria-describedby="emailHelp" placeholder="Enter email"> 
					<small id="emailHelp" class="form-text text-muted">We'll never
					share your email with anyone else.</small>
			</div>
	
	
			<div class="form-group">
				<input type="text" class="form-control" id="sujet" placeholder="sujet">
			</div>
	
			<div class="form-group">
				<textarea type="text" class="form-control" id="contenu" rows="10" placeholder="votre message">
				</textarea>
			</div>
	
			<div class="text-right">
				<button type="submit" class="btn btn-primary data-toggle=" modal" data-target="#exampleModal"" >
				envoyer
				</button>
			</div>
	
		</form>
		
		 <!-- footer -->
    	<jsp:include page="footer.jsp" />
	
	</body>
</html>