<%@page import="model.Client"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Index</title>
</head>
<body>

<!-- Header -->
<jsp:include page="WEB-INF/navbar.jsp"/>

<div style="height: 100px;"></div>

	<main>
	
	 <c:if test="${sessionScope.estConnecte}">
		<h2 class="text-center">
		Bonjour
		<c:out value = "${sessionScope.client.nom }"/> 
		<c:out value = "${sessionScope.client.prenom }"/>
		</h2>
	</c:if>

	<div class="pt-5 text-center">
		<div id="demo" class="carousel slide" data-ride="carousel"
			data-interval="2000">

			<!-- Indicators -->
			<ul class="carousel-indicators">
				<li data-slide-to="0" class="active"></li>
				<li data-slide-to="1"></li>
				<li data-slide-to="2"></li>
			</ul>

			<!-- The slideshow -->
			<div class="carousel-inner ">
				<div class="carousel-item active">
					<a href="RootagePage?destination=catalogue"> <img src="img/nourriture/burger.png"
						alt="Colibri1">
					</a>
				</div>
				<div class="carousel-item">
					<a href="RootagePage?destination=catalogue"> <img src="img/nourriture/frites.png"
						alt="Colibri2">
					</a>
				</div>
				<div class="carousel-item">
					<a href="RootagePage?destination=catalogue"> <img
						src="img/nourriture/fromage.png" alt="Colibri3">
					</a>
				</div>
			</div>

			<!-- Left and right controls -->
			<a class="carousel-control-prev" href="#demo" data-slide="prev">
				<span class="carousel-control-prev-icon"></span>
			</a> <a class="carousel-control-next" href="#demo" data-slide="next">
				<span class="carousel-control-next-icon"></span>
			</a>

		</div>
	</div>

	<div class="container-fluid mt-5 text-center">
		<a href="RootagePage?destination=catalogue"> <img src="img/nourriture/glace.png"
			alt="Colibri1" class="w-25 mx-lg-5">
		</a> <a href="RootagePage?destination=catalogue"> <img src="img/nourriture/poulet.png"
			alt="Colibri2" class="w-25 mx-lg-5">
		</a> <a href="RootagePage?destination=catalogue"> <img src="img/nourriture/steak.png"
			alt="Colibri3" class=" w-25 mx-lg-5">
		</a>

	</div>

	</main>
	
	<!-- footer -->
	<jsp:include page="WEB-INF/footer.jsp" />

</body>
</html>