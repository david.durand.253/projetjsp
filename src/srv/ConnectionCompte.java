package srv;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Client;
import model.Ligne;
import model.Facture;
import dao.DaoClient;

/**
 * Servlet implementation class ConnectionCompte
 */
@WebServlet("/ConnectionCompte")
public class ConnectionCompte extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConnectionCompte() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		boolean estConnecte = false;
		estConnecte = !estConnecte;
		
		HttpSession session = request.getSession();
		
		Client client = new Client(0, "", "", "", "");
    	Facture facture = new Facture();
		
		String stringId = request.getParameter("id"); 
		int	id = 0;
		
		String	password = request.getParameter("password");
		
		try {
			
			id = Integer.parseInt(stringId);
			
			client = new DaoClient().selectById(id, password);

			
			facture.setNom(client.getNom());
			
			ArrayList<Ligne> lignes = new ArrayList<Ligne>();
			facture.setLignes(lignes);
	
			session.setAttribute("client", client);
			session.setAttribute("estConnecte", estConnecte);
			session.setAttribute("facture", facture);

			request.getRequestDispatcher("index.jsp").forward(request, response);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			session.setAttribute("estConnect", estConnecte);
			request.getRequestDispatcher("WEB-INF/connection.jsp?erreur=true").forward(request, response);
		}


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
