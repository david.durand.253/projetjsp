package srv;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DaoArticle;
import model.Article;
import model.Facture;
import model.Ligne;

/**
 * Servlet implementation class Plus
 */
@WebServlet("/Plus")
public class Plus extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Plus() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DaoArticle dao = new DaoArticle();
		int ref = Integer.parseInt(request.getParameter("ref"));
		int quantite = Integer.parseInt(request.getParameter("quantite"));
		HttpSession session = request.getSession();
		Facture facture = (Facture) session.getAttribute("facture");
	
			try {
				Article article = dao.selectByRef(ref);
				Ligne nouvelleLigne = new Ligne(article, quantite + 1);
				facture.changeLignePlus(nouvelleLigne);
				session.setAttribute("facture", facture);

			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		request.getRequestDispatcher("WEB-INF/maCommande.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
