package srv;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DaoCommande;
import model.Client;
import model.Commande;
import model.Facture;
import model.Ligne;

/**
 * Servlet implementation class CreerCommandeDansBase
 */
@WebServlet("/CreerCommandeDansBase")
public class CreerCommandeDansBase extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreerCommandeDansBase() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		Client client = (Client)session.getAttribute("client");
		Facture facture = (Facture)session.getAttribute("facture");
		
		String infos = " ";
		
		for(Ligne ligne : facture.getLignes()) {
			infos += ligne.getQuantite() + "/"  + ligne.getArticle().getRef() + " ";
		}
		
		// id commande pas utilise en insert (Auto increment)
		Commande commande = new Commande(1, 
				client.getId(), 
				new Date().toString(), 
				facture.getTotalFacture(), 
				infos);
		
		try {
			new DaoCommande().insert(commande);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		request.getRequestDispatcher("RootagePage?destination=commandeValidee").forward(request, response);		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
