package srv;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DaoArticle;
import model.Article;
import model.Facture;
import model.Ligne;

/**
 * Servlet implementation class ajoutPanier
 */
@WebServlet("/ajoutPanier")
public class ajoutPanier extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ajoutPanier() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int ref = Integer.parseInt(request.getParameter("listeDeroul"));
		
		DaoArticle a = new DaoArticle();
		
		Article article;
		
		HttpSession session = request.getSession();
		Facture facture = (Facture) session.getAttribute("facture");
		
		try {
			article = a.selectByRef(ref);
			int quantite = Integer.parseInt(request.getParameter("quantite"));
			Ligne ligne = new Ligne (article, quantite);
			facture.addLigne(ligne);
			session.setAttribute("facture", facture);

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		request.getRequestDispatcher("WEB-INF/maCommande.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
