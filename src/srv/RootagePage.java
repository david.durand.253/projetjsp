package srv;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class RootagePage
 */
@WebServlet("/RootagePage")
public class RootagePage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RootagePage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String destination = "";
		
		if( request.getParameter("destination") != null)
			destination = request.getParameter("destination");

		switch(destination) {
		
		case "catalogue":
			request.getRequestDispatcher("WEB-INF/catalogue.jsp").forward(request, response);
			break;
			
		case "connection":
			request.getRequestDispatcher("WEB-INF/connection.jsp").forward(request, response);
			break;
			
		case "inscription":
			request.getRequestDispatcher("WEB-INF/inscription.jsp").forward(request, response);
			break;
			
		case "maCommande":
			request.getRequestDispatcher("WEB-INF/maCommande.jsp").forward(request, response);
			break;
			
		case "apropos":
			request.getRequestDispatcher("WEB-INF/apropos.jsp").forward(request, response);
			break;
			
		case "contact":
			request.getRequestDispatcher("WEB-INF/contact.jsp").forward(request, response);
			break;
			
		case "recapCommande":
			request.getRequestDispatcher("WEB-INF/recapCommande.jsp").forward(request, response);
			break;
			
		case "commandeValidee":
			request.getRequestDispatcher("WEB-INF/commandeValidee.jsp").forward(request, response);
			break;
				
		default :
			request.getRequestDispatcher("index.jsp").forward(request, response);
			break;
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
