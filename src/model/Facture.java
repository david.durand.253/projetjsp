package model;

import java.util.ArrayList;

public class Facture {

	private String nom;
	private ArrayList<Ligne> lignes;
	private int totalFacture;

	public Facture(String nom, ArrayList<Ligne> lignes) {
		super();
		this.nom = nom;
		this.lignes = lignes;
		for (Ligne l : lignes)
			this.totalFacture += (l.totalLigne);

	}

	public Facture() {
		super();
	}

	// Accesseurs :
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public ArrayList<Ligne> getLignes() {
		return lignes;
	}

	public void addLigne(Ligne ligne) {
		boolean ligneTrouvee = false;
		for (int index = 0; index < this.lignes.size(); index++) {
			if (this.lignes.get(index).compareTo(ligne) == 0) {
				
				int quantite = this.lignes.get(index).getQuantite();
				quantite += ligne.getQuantite();
				this.lignes.get(index).setQuantite(quantite);
				
				int prixTotal = this.lignes.get(index).getTotalLigne();
				prixTotal += ligne.getTotalLigne();
				this.lignes.get(index).setTotalLigne(prixTotal);
				
				this.totalFacture +=ligne.getTotalLigne();
				
				ligneTrouvee = true;
				break;
			}
		}
		if (!ligneTrouvee){
			this.lignes.add(ligne);
			this.totalFacture += ligne.getTotalLigne();
		}
	}
	public void changeLigneMoins(Ligne ligne) {
		boolean ligneTrouvee = false;
		for (int index = 0; index < this.lignes.size(); index++) {
			if (this.lignes.get(index).compareTo(ligne) == 0) {
				if(ligne.getQuantite()==0)
					this.lignes.remove(index);
				else{
				this.lignes.set(index, ligne);
				}
				this.totalFacture -= ligne.getArticle().getTarif();
				ligneTrouvee = true;
				break;
				
			}
		}
		if (!ligneTrouvee){
			this.lignes.add(ligne);
			this.totalFacture += ligne.getTotalLigne();
		}
	}
	public void changeLignePlus(Ligne ligne) {
		boolean ligneTrouvee = false;
		for (int index = 0; index < this.lignes.size(); index++) {
			if (this.lignes.get(index).compareTo(ligne) == 0) {
					this.lignes.set(index, ligne);
				this.totalFacture += ligne.getArticle().getTarif();
				ligneTrouvee = true;
				break;
				
			}
		}
		if (!ligneTrouvee){
			this.lignes.add(ligne);
			this.totalFacture += ligne.getTotalLigne();
		}
	}

	public void setLignes(ArrayList<Ligne> lignes) {
		this.lignes = lignes;
	}

	public int getTotalFacture() {
		return totalFacture;
	}

	public void setTotalFacture(int totalFacture) {
		this.totalFacture = totalFacture;
	}

	@Override
	public String toString() {
		return "Facture [nom=" + nom + ", lignes=" + lignes + ", totalFacture=" + totalFacture + "]";
	}

}
