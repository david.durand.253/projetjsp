package model;

public class Client {

	private int id;
	private String password;
	private String nom;
	private String prenom;
	private String complement;

	public Client() {
	}

	public Client(int id, String password, String nom, String prenom, String complement) {
		this.id = id;
		this.password = password;
		this.nom = nom;
		this.prenom = prenom;
		this.complement = complement;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getComplement() {
		return complement;
	}

	public void setComplement(String complement) {
		this.complement = complement;
	}

	@Override
	public String toString() {
		return "Client [id=" + id + ", password=" + password + ",nom=" + nom + ", prenom=" + prenom + ", complement="
				+ complement + "]";
	}

}
