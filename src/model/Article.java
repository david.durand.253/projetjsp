package model;

public class Article implements Comparable<Article>{
	private int ref;
	private String nom;
	private String description;
	private int tarif;
	private String url;
	
	public Article() {
		super();
	}
	public Article(int ref, String nom, String description, int tarif, String url) {
		super();
		this.ref = ref;
		this.nom = nom;
		this.description = description;
		this.tarif = tarif;
		this.url = url;
	}
	public int getRef() {
		return ref;
	}
	public void setRef(int ref) {
		this.ref = ref;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getTarif() {
		return tarif;
	}
	public void setTarif(int tarif) {
		this.tarif = tarif;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	@Override
	public String toString() {
		return "Article [ref=" + ref + ", nom=" + nom + ", description=" + description + ", tarif=" + tarif + ", url="
				+ url + "]";
	}
	
	public int compareTo(Article article) {
		return ((Integer)this.ref).compareTo(article.ref);
	}

	
	

}
