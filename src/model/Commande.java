package model;

public class Commande {
	private int id;
	private int idclient;
	private String jour;
	private int prixtotal;
	private String infos;

	public Commande() {
		super();
	}

	public Commande(int id, int idclient, String jour, int prixtotal, String infos) {
		super();
		this.id = id;
		this.idclient = idclient;
		this.jour = jour;
		this.prixtotal = prixtotal;
		this.infos = infos;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdclient() {
		return idclient;
	}

	public void setIdclient(int idclient) {
		this.idclient = idclient;
	}

	public String getJour() {
		return jour;
	}

	public void setJour(String jour) {
		this.jour = jour;
	}

	public int getPrixtotal() {
		return prixtotal;
	}

	public void setPrixtotal(int prixtotal) {
		this.prixtotal = prixtotal;
	}

	public String getInfos() {
		return infos;
	}

	public void setInfos(String infos) {
		this.infos = infos;
	}

	@Override
	public String toString() {
		return "Commande [id=" + id + ", idclient=" + idclient + ", jour=" + jour + ", prixtotal=" + prixtotal
				+ ", infos=" + infos + "]";
	}

}
