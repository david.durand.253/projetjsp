package model;

public class Ligne implements Comparable<Ligne>{

	private Article article ;
	private int quantite ;
	int totalLigne ;
	
	
	
	
	public Ligne(Article article, int quantite) {
		super();
		this.article = article;
		this.quantite = quantite;
		this.totalLigne = quantite * this.article.getTarif();
	}

	public Ligne() {
		super();
	}
	
	

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public int getQuantite() {
		return quantite;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}

	public int getTotalLigne() {
		return totalLigne;
	}

	public void setTotalLigne(int totalLigne) {
		this.totalLigne = totalLigne;
	}

	@Override
	public String toString() {
		return "Ligne [article=" + article + ", quantite=" + quantite + ", totalLigne=" + totalLigne + "]";
	}

	public int compareTo(Ligne ligne) {
		return this.article.compareTo(ligne.article);
	}
	
	
	
	
	
	
	
	
	
	
	

	
	
	
	
	
}
