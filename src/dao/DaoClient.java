package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


import model.Client;

public class DaoClient {
	public Client selectById(int id, String password) throws ClassNotFoundException, SQLException {
		Client client = null;
		String sql = "select * from clients where id=? and password=?";
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/projetjsp", "root", "root");
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setInt(1, id);
		ps.setString(2, password);
		ResultSet rs = ps.executeQuery();

		while (rs.next()) {
			client = new Client(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
		}
		conn.close();
		return client;
	}


	public void insert(Client client) throws ClassNotFoundException, SQLException {
		String sql = "insert into clients values (?,?,?,?,?)";
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/projetjsp", "root", "root");
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setInt(1, client.getId());
		ps.setString(2, client.getPassword());
		ps.setString(3, client.getNom());
		ps.setString(4, client.getPrenom());
		ps.setString(5, client.getComplement());
		ps.executeUpdate();
		conn.close();
	}
}
