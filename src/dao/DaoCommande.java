package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.Commande;

public class DaoCommande {
	public Commande selectByid(int id) throws ClassNotFoundException, SQLException {
		Commande commande = null;
		String sql = "select * from commandes where id=" + id;
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/projetjsp", "root", "root");
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(sql);

		while (rs.next()) {
			commande = new Commande(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getInt(4), rs.getString(5));
		}
		conn.close();
		return commande;
	}

	public ArrayList<Commande> selectAll() throws ClassNotFoundException, SQLException {
		ArrayList<Commande> articles = new ArrayList<Commande>();
		String sql = "select * from commandes";
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/projetjsp", "root", "root");
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(sql);

		while (rs.next()) {
			Commande article = new Commande(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getInt(4), rs.getString(5));
			articles.add(article);
		}
		conn.close();
		return articles;
	}

	public void insert(Commande commande) throws ClassNotFoundException, SQLException {
		String sql = "insert into commandes (idClient, jour, prixtotal, infos) values (?,?,?,?)";
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/projetjsp", "root", "root");
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setInt(1, commande.getIdclient());
		ps.setString(2, commande.getJour());
		ps.setInt(3, commande.getPrixtotal());
		ps.setString(4, commande.getInfos());
		ps.executeUpdate();
		conn.close();
	}

}
