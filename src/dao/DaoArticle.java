package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.Article;

public class DaoArticle {

	public Article selectByRef(int ref)throws ClassNotFoundException, SQLException {
		Article article = null;
		String sql = "select * from articles where ref="+ref;
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/projetjsp", "root", "root");
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(sql);
		
		while(rs.next()){
			article = new Article(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4), rs.getString(5));
		}
		conn.close();
		return article;
	}
	public ArrayList<Article> selectAll()throws ClassNotFoundException, SQLException {
		ArrayList<Article> articles = new ArrayList<Article>();
		String sql = "select * from articles";
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/projetjsp", "root", "root");
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(sql);
		
		while(rs.next()){
			Article article = new Article(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4), rs.getString(5));
			articles.add(article);
		}
		conn.close();
		return articles;
	}
	public ArrayList<Article> selectAllAliments()throws ClassNotFoundException, SQLException {
		ArrayList<Article> articles = new ArrayList<Article>();
		String sql = "select * from articles where ref between 100 and 199";
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/projetjsp", "root", "root");
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(sql);
		
		while(rs.next()){
			Article article = new Article(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4), rs.getString(5));
			articles.add(article);
		}
		conn.close();
		return articles;
	}
	public ArrayList<Article> selectAllBoissons()throws ClassNotFoundException, SQLException {
		ArrayList<Article> articles = new ArrayList<Article>();
		String sql = "select * from articles where ref between 200 and 299";;
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/projetjsp", "root", "root");
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(sql);
		
		while(rs.next()){
			Article article = new Article(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4), rs.getString(5));
			articles.add(article);
		}
		conn.close();
		return articles;
	}
	public ArrayList<Article> selectAllMenagers()throws ClassNotFoundException, SQLException {
		ArrayList<Article> articles = new ArrayList<Article>();
		String sql = "select * from articles where ref between 300 and 399";
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/projetjsp", "root", "root");
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(sql);
		
		while(rs.next()){
			Article article = new Article(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4), rs.getString(5));
			articles.add(article);
		}
		conn.close();
		return articles;
	}
}
