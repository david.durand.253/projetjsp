package lst;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import dao.DaoArticle;
import model.Article;

/**
 * Application Lifecycle Listener implementation class ListenAfficherArticles
 *
 */
@WebListener
public class ListenAfficherArticles implements ServletContextListener {

    /**
     * Default constructor. 
     */
    public ListenAfficherArticles() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent arg0)  { 
         // TODO Auto-generated method stub
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent arg0)  { 
    	 ServletContext app = arg0.getServletContext();
 		
 		DaoArticle x = new DaoArticle();
 		
 		ArrayList<Article> toutCatalogue = new ArrayList<Article> () ;
 		ArrayList<Article> aliments = new ArrayList<Article> () ;
 		ArrayList<Article> boissons = new ArrayList<Article> () ;
 		ArrayList<Article> menagers = new ArrayList<Article> () ;
 		
 		        try {
 		          toutCatalogue = x.selectAll();
 		           aliments = x.selectAllAliments(); // -- 100 < < 200
 		           boissons =  x.selectAllBoissons(); // -- 200 < < 300
 		           menagers =  x.selectAllMenagers(); // -- 300 < < 400
 		       } catch (ClassNotFoundException e) {
 		           // TODO Auto-generated catch block
 		           e.printStackTrace();
 		       } catch (SQLException e) {
 		           // TODO Auto-generated catch block
 		           e.printStackTrace();
 		       }
 		        
 		   	ArrayList<Article> halfAlimentsA = new ArrayList<Article> () ;
 		   	ArrayList<Article> halfAlimentsB = new ArrayList<Article> () ;
 		   	
 		   	for(int i = 0; i < 4 ; i++) {
 		   		Article a =  aliments.get(i);
 		   		halfAlimentsA.add(a);
 		   	}
 		   	
 		   	for(int i = 4; i < 8 ; i++) {
 		   		Article a =  aliments.get(i);
 		   		halfAlimentsB.add(a);
 		   	}
 		   	
 			ArrayList<Article> halfBoissonsA = new ArrayList<Article> () ;
 		   	ArrayList<Article> halfBoissonsB = new ArrayList<Article> () ;
 		   	
 		   	for(int i = 0; i < 4 ; i++) {
 		   		Article a =  boissons.get(i);
 		   		halfBoissonsA.add(a);
 		   	}
 		   	
 		   	for(int i = 4; i < 8 ; i++) {
 		   		Article a =  boissons.get(i);
 		   		halfBoissonsB.add(a);
 		   	}
 		   	
 			ArrayList<Article> halfMenagersA = new ArrayList<Article> () ;
 		   	ArrayList<Article> halfMenagersB = new ArrayList<Article> () ;
 		   	
 		   	for(int i = 0; i < 4 ; i++) {
 		   		Article a =  menagers.get(i);
 		   		halfMenagersA.add(a);
 		   	}
 		   	
 		   	for(int i = 4; i < 8 ; i++) {
 		   		Article a =  menagers.get(i);
 		   		halfMenagersB.add(a);
 		   	}
 		   	

 		 app.setAttribute("toutCatalogue", toutCatalogue);
         app.setAttribute("aliments", aliments);
         	app.setAttribute("halfAlimentsA", halfAlimentsA);
         	app.setAttribute("halfAlimentsB", halfAlimentsB);
         app.setAttribute("boissons", boissons);
         	app.setAttribute("halfBoissonsA", halfBoissonsA);
         	app.setAttribute("halfBoissonsB", halfBoissonsB);
         app.setAttribute("menagers", menagers);
         	app.setAttribute("halfMenagersA", halfMenagersA);
         	app.setAttribute("halfMenagersB", halfMenagersB);

    }
	
}
