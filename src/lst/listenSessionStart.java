package lst;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import model.Client;

/**
 * Application Lifecycle Listener implementation class listenSessionStart
 *
 */
@WebListener
public class listenSessionStart implements HttpSessionListener {

    /**
     * Default constructor. 
     */
    public listenSessionStart() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see HttpSessionListener#sessionCreated(HttpSessionEvent)
     */
    public void sessionCreated(HttpSessionEvent arg0)  { 

    	HttpSession session = arg0.getSession();
    	
    	boolean estConnecte = false;
    	Client client = new Client(0, "", "", "", "");
 
    	session.setAttribute("client", client);
		session.setAttribute("estConnecte", estConnecte);
    }

	/**
     * @see HttpSessionListener#sessionDestroyed(HttpSessionEvent)
     */
    public void sessionDestroyed(HttpSessionEvent arg0)  { 
         // TODO Auto-generated method stub
    }
	
}
